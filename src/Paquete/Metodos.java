package Paquete;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
public class Metodos
{
    public Orden InsertarOrden()
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Orden orden = new Orden();
        java.util.Date utilDate=new java.util.Date();
        java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());
        orden.setFechaOrden(sqlDate);
        try
        {
            em.persist(orden);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
            System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return orden;
    }
    public Producto InsertarProduc(String descripcion,Float preciopr)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Producto producto1 = new Producto();
        producto1.setDescripcion(descripcion);
        Float precio=Float.valueOf(preciopr);
        producto1.setPrecio(precio);
        try
        {
            em.persist(producto1);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
            System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return producto1;
    }
    public Lineaorden InsertarLinOrden(Orden orden,Producto producto,int cantidad)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
                
        Lineaorden lineaorden1 = new Lineaorden(orden.getIdOrden(),producto.getIdProducto());
        lineaorden1.setCantidad(cantidad);
        
        try
        {
            em.persist(lineaorden1);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
 	           System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return lineaorden1;
    }
    public void Mostrar(Orden orden,Producto producto,Lineaorden lineaorden)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Collection<Orden> listaOrden;
        listaOrden = em.createNamedQuery("Orden.findAll").getResultList();
        System.out.println("ORDEN");
        System.out.println("                                               ");
        for (Orden lo : listaOrden)
        {
            System.out.println("id orden: "+lo.getIdOrden()+"\nFecha orden: "
                    +lo.getFechaOrden());
            System.out.println("//////////////////////////////////////////");
            System.out.println("                                               ");        
        }        
        System.out.println("PRODUCTO");
        System.out.println("                                               ");
        Collection<Producto> listaProducto;
        listaProducto = em.createNamedQuery("Producto.findAll").getResultList();

        for (Producto lp : listaProducto)
        {
            System.out.println("id producto: "+lp.getIdProducto()+"\n descripcion producto: "
                    +lp.getDescripcion()+"\n precio: "
                    +lp.getPrecio());
            System.out.println("**********************************************");
            System.out.println("                                               ");
        }
        System.out.println("LINEA ORDEN");
        System.out.println("                                               ");
        Collection<Lineaorden> listaLineaorden;
        listaLineaorden = em.createNamedQuery("Lineaorden.findAll").getResultList();

        for (Lineaorden lo : listaLineaorden)
        {
            System.out.println("id orden: "+lo.getOrden().getIdOrden()+"\n id producto: "
                    +lo.getProducto().getIdProducto()+"\n cantidad: "
                    +lo.getCantidad());
            System.out.println("--------------------------------------------------");
            System.out.println("                                               ");
        }      
    }
}

































/*
        System.out.println("------------Datos Orden-------------");
        System.out.println("ID orden: "+orden.getIdOrden());
        System.out.println("ID orden: "+orden.getFechaOrden());
        System.out.println("------------Datos Producto----------");
        System.out.println("ID orden: "+producto.getIdProducto());
        System.out.println("ID orden: "+producto.getDescripcion());
        System.out.println("ID orden: "+producto.getPrecio());
        System.out.println("------------Datos Lineaorden--------");
        System.out.println("ID orden: "+lineaorden.getLineaordenPK().getIdOrden());
        System.out.println("ID orden: "+lineaorden.getLineaordenPK().getIdProducto());
        System.out.println("ID orden: "+lineaorden.getCantidad());
        */